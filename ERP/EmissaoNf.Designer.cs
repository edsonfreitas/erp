﻿namespace ERP
{
    partial class formEmissaoNF
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupDadosCadastrais = new System.Windows.Forms.GroupBox();
            this.label13 = new System.Windows.Forms.Label();
            this.txtCep = new System.Windows.Forms.TextBox();
            this.txtIe = new System.Windows.Forms.MaskedTextBox();
            this.txtCnpjCpf = new System.Windows.Forms.MaskedTextBox();
            this.txtUF = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.txtCodMun = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.txtMunicipio = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.txtBairro = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.txtComplemento = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.txtNumero = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.txtLogradouro = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.txtRazaoSocial = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtNomeFantasia = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txtID = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.btnPesquisar = new System.Windows.Forms.Button();
            this.label14 = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label19 = new System.Windows.Forms.Label();
            this.textBox7 = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.textBox4 = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.textBox5 = new System.Windows.Forms.TextBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label18 = new System.Windows.Forms.Label();
            this.textBox6 = new System.Windows.Forms.TextBox();
            this.btnEmitirNF = new System.Windows.Forms.Button();
            this.btnLimparDados = new System.Windows.Forms.Button();
            this.groupDadosCadastrais.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupDadosCadastrais
            // 
            this.groupDadosCadastrais.AutoSize = true;
            this.groupDadosCadastrais.Controls.Add(this.label13);
            this.groupDadosCadastrais.Controls.Add(this.txtCep);
            this.groupDadosCadastrais.Controls.Add(this.txtIe);
            this.groupDadosCadastrais.Controls.Add(this.txtCnpjCpf);
            this.groupDadosCadastrais.Controls.Add(this.txtUF);
            this.groupDadosCadastrais.Controls.Add(this.label12);
            this.groupDadosCadastrais.Controls.Add(this.txtCodMun);
            this.groupDadosCadastrais.Controls.Add(this.label11);
            this.groupDadosCadastrais.Controls.Add(this.txtMunicipio);
            this.groupDadosCadastrais.Controls.Add(this.label10);
            this.groupDadosCadastrais.Controls.Add(this.txtBairro);
            this.groupDadosCadastrais.Controls.Add(this.label9);
            this.groupDadosCadastrais.Controls.Add(this.txtComplemento);
            this.groupDadosCadastrais.Controls.Add(this.label8);
            this.groupDadosCadastrais.Controls.Add(this.txtNumero);
            this.groupDadosCadastrais.Controls.Add(this.label7);
            this.groupDadosCadastrais.Controls.Add(this.txtLogradouro);
            this.groupDadosCadastrais.Controls.Add(this.label6);
            this.groupDadosCadastrais.Controls.Add(this.label5);
            this.groupDadosCadastrais.Controls.Add(this.txtRazaoSocial);
            this.groupDadosCadastrais.Controls.Add(this.label4);
            this.groupDadosCadastrais.Controls.Add(this.txtNomeFantasia);
            this.groupDadosCadastrais.Controls.Add(this.label3);
            this.groupDadosCadastrais.Controls.Add(this.label2);
            this.groupDadosCadastrais.Controls.Add(this.txtID);
            this.groupDadosCadastrais.Controls.Add(this.label1);
            this.groupDadosCadastrais.Location = new System.Drawing.Point(12, 65);
            this.groupDadosCadastrais.Name = "groupDadosCadastrais";
            this.groupDadosCadastrais.Size = new System.Drawing.Size(480, 291);
            this.groupDadosCadastrais.TabIndex = 20;
            this.groupDadosCadastrais.TabStop = false;
            this.groupDadosCadastrais.Text = "Dados do Cliente";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(82, 30);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(63, 17);
            this.label13.TabIndex = 56;
            this.label13.Text = "CNPJ/CPF";
            // 
            // txtCep
            // 
            this.txtCep.Location = new System.Drawing.Point(404, 98);
            this.txtCep.MaxLength = 8;
            this.txtCep.Name = "txtCep";
            this.txtCep.Size = new System.Drawing.Size(70, 25);
            this.txtCep.TabIndex = 7;
            this.txtCep.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtIe
            // 
            this.txtIe.CutCopyMaskFormat = System.Windows.Forms.MaskFormat.ExcludePromptAndLiterals;
            this.txtIe.Location = new System.Drawing.Point(208, 50);
            this.txtIe.Name = "txtIe";
            this.txtIe.Size = new System.Drawing.Size(120, 25);
            this.txtIe.TabIndex = 4;
            this.txtIe.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtIe.TextMaskFormat = System.Windows.Forms.MaskFormat.ExcludePromptAndLiterals;
            // 
            // txtCnpjCpf
            // 
            this.txtCnpjCpf.CutCopyMaskFormat = System.Windows.Forms.MaskFormat.ExcludePromptAndLiterals;
            this.txtCnpjCpf.Location = new System.Drawing.Point(82, 50);
            this.txtCnpjCpf.Name = "txtCnpjCpf";
            this.txtCnpjCpf.Size = new System.Drawing.Size(120, 25);
            this.txtCnpjCpf.TabIndex = 3;
            this.txtCnpjCpf.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtCnpjCpf.TextMaskFormat = System.Windows.Forms.MaskFormat.ExcludePromptAndLiterals;
            // 
            // txtUF
            // 
            this.txtUF.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtUF.Location = new System.Drawing.Point(368, 242);
            this.txtUF.MaxLength = 2;
            this.txtUF.Name = "txtUF";
            this.txtUF.Size = new System.Drawing.Size(30, 25);
            this.txtUF.TabIndex = 13;
            this.txtUF.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(374, 222);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(23, 17);
            this.label12.TabIndex = 55;
            this.label12.Text = "UF";
            this.label12.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // txtCodMun
            // 
            this.txtCodMun.Location = new System.Drawing.Point(404, 242);
            this.txtCodMun.MaxLength = 7;
            this.txtCodMun.Name = "txtCodMun";
            this.txtCodMun.Size = new System.Drawing.Size(70, 25);
            this.txtCodMun.TabIndex = 14;
            this.txtCodMun.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(404, 222);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(68, 17);
            this.label11.TabIndex = 54;
            this.label11.Text = "Cód. Mun.";
            // 
            // txtMunicipio
            // 
            this.txtMunicipio.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtMunicipio.Location = new System.Drawing.Point(6, 242);
            this.txtMunicipio.MaxLength = 30;
            this.txtMunicipio.Name = "txtMunicipio";
            this.txtMunicipio.Size = new System.Drawing.Size(356, 25);
            this.txtMunicipio.TabIndex = 12;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(6, 222);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(65, 17);
            this.label10.TabIndex = 53;
            this.label10.Text = "Municipio";
            // 
            // txtBairro
            // 
            this.txtBairro.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtBairro.Location = new System.Drawing.Point(208, 194);
            this.txtBairro.MaxLength = 15;
            this.txtBairro.Name = "txtBairro";
            this.txtBairro.Size = new System.Drawing.Size(266, 25);
            this.txtBairro.TabIndex = 11;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(208, 174);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(43, 17);
            this.label9.TabIndex = 52;
            this.label9.Text = "Bairro";
            // 
            // txtComplemento
            // 
            this.txtComplemento.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtComplemento.Location = new System.Drawing.Point(6, 194);
            this.txtComplemento.MaxLength = 15;
            this.txtComplemento.Name = "txtComplemento";
            this.txtComplemento.Size = new System.Drawing.Size(196, 25);
            this.txtComplemento.TabIndex = 10;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(6, 174);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(90, 17);
            this.label8.TabIndex = 51;
            this.label8.Text = "Complemento";
            // 
            // txtNumero
            // 
            this.txtNumero.Location = new System.Drawing.Point(404, 146);
            this.txtNumero.MaxLength = 5;
            this.txtNumero.Name = "txtNumero";
            this.txtNumero.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtNumero.Size = new System.Drawing.Size(70, 25);
            this.txtNumero.TabIndex = 9;
            this.txtNumero.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(404, 126);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(56, 17);
            this.label7.TabIndex = 49;
            this.label7.Text = "Numero";
            // 
            // txtLogradouro
            // 
            this.txtLogradouro.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtLogradouro.Location = new System.Drawing.Point(6, 146);
            this.txtLogradouro.MaxLength = 45;
            this.txtLogradouro.Name = "txtLogradouro";
            this.txtLogradouro.Size = new System.Drawing.Size(392, 25);
            this.txtLogradouro.TabIndex = 8;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(6, 126);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(78, 17);
            this.label6.TabIndex = 46;
            this.label6.Text = "Logradouro";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(404, 78);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(30, 17);
            this.label5.TabIndex = 43;
            this.label5.Text = "CEP";
            // 
            // txtRazaoSocial
            // 
            this.txtRazaoSocial.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtRazaoSocial.Location = new System.Drawing.Point(6, 98);
            this.txtRazaoSocial.MaxLength = 35;
            this.txtRazaoSocial.Name = "txtRazaoSocial";
            this.txtRazaoSocial.Size = new System.Drawing.Size(392, 25);
            this.txtRazaoSocial.TabIndex = 6;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(6, 78);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(82, 17);
            this.label4.TabIndex = 41;
            this.label4.Text = "Razão Social";
            // 
            // txtNomeFantasia
            // 
            this.txtNomeFantasia.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtNomeFantasia.Location = new System.Drawing.Point(334, 50);
            this.txtNomeFantasia.MaxLength = 15;
            this.txtNomeFantasia.Name = "txtNomeFantasia";
            this.txtNomeFantasia.Size = new System.Drawing.Size(140, 25);
            this.txtNomeFantasia.TabIndex = 5;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(339, 30);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(95, 17);
            this.label3.TabIndex = 37;
            this.label3.Text = "Nome Fantasia";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(208, 30);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(112, 17);
            this.label2.TabIndex = 33;
            this.label2.Text = "Inscrição Estadual";
            // 
            // txtID
            // 
            this.txtID.Location = new System.Drawing.Point(6, 50);
            this.txtID.Name = "txtID";
            this.txtID.Size = new System.Drawing.Size(70, 25);
            this.txtID.TabIndex = 1;
            this.txtID.TabStop = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 30);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(20, 17);
            this.label1.TabIndex = 27;
            this.label1.Text = "ID";
            // 
            // btnPesquisar
            // 
            this.btnPesquisar.Location = new System.Drawing.Point(12, 12);
            this.btnPesquisar.Name = "btnPesquisar";
            this.btnPesquisar.Size = new System.Drawing.Size(100, 47);
            this.btnPesquisar.TabIndex = 2;
            this.btnPesquisar.Text = "Pesquisar Cliente";
            this.btnPesquisar.UseVisualStyleBackColor = true;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(396, 8);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(76, 17);
            this.label14.TabIndex = 3;
            this.label14.Text = "Numero NF";
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(386, 28);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(100, 25);
            this.textBox1.TabIndex = 4;
            this.textBox1.TabStop = false;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label19);
            this.groupBox1.Controls.Add(this.textBox7);
            this.groupBox1.Controls.Add(this.label17);
            this.groupBox1.Controls.Add(this.textBox4);
            this.groupBox1.Controls.Add(this.label16);
            this.groupBox1.Controls.Add(this.textBox3);
            this.groupBox1.Controls.Add(this.label15);
            this.groupBox1.Controls.Add(this.textBox2);
            this.groupBox1.Location = new System.Drawing.Point(12, 362);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(328, 187);
            this.groupBox1.TabIndex = 21;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Dados da Fatura";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(6, 78);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(84, 17);
            this.label19.TabIndex = 15;
            this.label19.Text = "Observações";
            // 
            // textBox7
            // 
            this.textBox7.Location = new System.Drawing.Point(6, 98);
            this.textBox7.Multiline = true;
            this.textBox7.Name = "textBox7";
            this.textBox7.Size = new System.Drawing.Size(312, 83);
            this.textBox7.TabIndex = 18;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(218, 30);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(57, 17);
            this.label17.TabIndex = 10;
            this.label17.Text = "Valor R$";
            // 
            // textBox4
            // 
            this.textBox4.Location = new System.Drawing.Point(218, 50);
            this.textBox4.Name = "textBox4";
            this.textBox4.Size = new System.Drawing.Size(100, 25);
            this.textBox4.TabIndex = 17;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(112, 30);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(75, 17);
            this.label16.TabIndex = 8;
            this.label16.Text = "Vencimento";
            // 
            // textBox3
            // 
            this.textBox3.Location = new System.Drawing.Point(112, 50);
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new System.Drawing.Size(100, 25);
            this.textBox3.TabIndex = 16;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(6, 30);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(56, 17);
            this.label15.TabIndex = 6;
            this.label15.Text = "Emissão";
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(6, 50);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(100, 25);
            this.textBox2.TabIndex = 15;
            // 
            // textBox5
            // 
            this.textBox5.Location = new System.Drawing.Point(14, 23);
            this.textBox5.Name = "textBox5";
            this.textBox5.Size = new System.Drawing.Size(100, 25);
            this.textBox5.TabIndex = 1;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label18);
            this.groupBox2.Controls.Add(this.textBox6);
            this.groupBox2.Controls.Add(this.textBox5);
            this.groupBox2.Location = new System.Drawing.Point(125, 5);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(255, 54);
            this.groupBox2.TabIndex = 14;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Referência";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(120, 26);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(15, 17);
            this.label18.TabIndex = 15;
            this.label18.Text = "à";
            // 
            // textBox6
            // 
            this.textBox6.Location = new System.Drawing.Point(141, 23);
            this.textBox6.Name = "textBox6";
            this.textBox6.Size = new System.Drawing.Size(100, 25);
            this.textBox6.TabIndex = 2;
            // 
            // btnEmitirNF
            // 
            this.btnEmitirNF.Location = new System.Drawing.Point(346, 502);
            this.btnEmitirNF.Name = "btnEmitirNF";
            this.btnEmitirNF.Size = new System.Drawing.Size(146, 47);
            this.btnEmitirNF.TabIndex = 19;
            this.btnEmitirNF.Text = "Emitir Nota Fiscal";
            this.btnEmitirNF.UseVisualStyleBackColor = true;
            // 
            // btnLimparDados
            // 
            this.btnLimparDados.Location = new System.Drawing.Point(346, 390);
            this.btnLimparDados.Name = "btnLimparDados";
            this.btnLimparDados.Size = new System.Drawing.Size(146, 47);
            this.btnLimparDados.TabIndex = 16;
            this.btnLimparDados.TabStop = false;
            this.btnLimparDados.Text = "Limpar Dados";
            this.btnLimparDados.UseVisualStyleBackColor = true;
            // 
            // formEmissaoNF
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 17F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(504, 561);
            this.Controls.Add(this.btnLimparDados);
            this.Controls.Add(this.btnEmitirNF);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.btnPesquisar);
            this.Controls.Add(this.groupDadosCadastrais);
            this.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "formEmissaoNF";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = ".:: Emissão de Nota Fiscal ::.";
            this.groupDadosCadastrais.ResumeLayout(false);
            this.groupDadosCadastrais.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupDadosCadastrais;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox txtCep;
        private System.Windows.Forms.MaskedTextBox txtIe;
        private System.Windows.Forms.MaskedTextBox txtCnpjCpf;
        private System.Windows.Forms.TextBox txtUF;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox txtCodMun;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox txtMunicipio;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox txtBairro;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox txtComplemento;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txtNumero;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtLogradouro;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtRazaoSocial;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtNomeFantasia;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtID;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnPesquisar;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.TextBox textBox7;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox textBox4;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.TextBox textBox5;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TextBox textBox6;
        private System.Windows.Forms.Button btnEmitirNF;
        private System.Windows.Forms.Button btnLimparDados;
    }
}