﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ERP.DAO
{
    /*
     * ClienteDAO trata-se de uma classe que herda de IDAO, que é uma interface, com comandos salvar, consultar e excluir.
     * Por ela será possivel utilizar as funções citadas
     */
    class ClienteDAO : IDAO<Cliente>
    {
        // ATRIBUTOS
        //
        private SqlConnection connection;   // Propriedade de conexão com o banco de dados
        private string sql = null;          // Propriedade de comandos SQL (SELECT, INSERT, UPDATE etc).
        private string msg = null;          // Propriedade para mensagens via MessageBox das operações realizadas pelos métodos. 
        private string titulo = null;       // Título das mensagens das operações realizadas.
        //
        // CONSTRUTOR
        //
        public ClienteDAO()
        {
            connection = new ConnectionFactory().GetConnection();   // Propriedade de conexão com o banco de dados, a string "connection" encontra-se em ConnectionFactory
        }
        //
        // MÉTODOS
        //
        // Consultar <List> -  Este método tem o intuíto de listar em um DataGridView os clientes cadastrados no banco.
        //
        public List<Cliente> Consultar()
        {
            sql = "SELECT * FROM Clientes";                 // Comando SQL que seleciona todos os cadastros da tabela Clientes
            List<Cliente> clientes = new List<Cliente>();   // Cria uma instancia List onde serão armazenados os dados obtidos na variavel "clientes"
            try
            {
                connection.Open();                                  // Inicia uma conexão com o banco de dados.
                SqlCommand cmd = new SqlCommand(sql, connection);   // Insere o comando SQL solicitado na variável "sql" após a conexão.
                SqlDataReader leitor = cmd.ExecuteReader();         // Lê e armazena os dados retornados da consulta na variável "leitor".

                while (leitor.Read())   // Enquanto (while) houver dados para o "leitor" ler e armazenar...
                {
                    Cliente cliente = new Cliente();                        // Instancia um novo cliente para que este esteja elegível a receber os dados

                    cliente.Id = (int)leitor["Id_Cliente"];                 // 
                    cliente.CnpjCpf = (string)leitor["CnpjCpf"];            //
                    cliente.Ie = (string)leitor["Ie"];                      //
                    cliente.NomeFantasia = (string)leitor["NomeFantasia"];  //
                    cliente.RazaoSocial = (string)leitor["RazaoSocial"];    //
                    cliente.Cep = (string)leitor["Cep"];                    //
                    cliente.Logradouro = (string)leitor["Logradouro"];      //
                    cliente.Numero = (string)leitor["Numero"];              //
                    cliente.Complemento = (string)leitor["Complemento"];    // Lê e armazena os dados nas respectivas colunas.
                    cliente.Bairro = (string)leitor["Bairro"];              //
                    cliente.Municipio = (string)leitor["Municipio"];        //
                    cliente.Uf = (string)leitor["Uf"];                      //
                    cliente.CodMun = (string)leitor["CodMun"];              //
                    cliente.Telefone = (string)leitor["Telefone"];          //
                    cliente.Celular = (string)leitor["Celular"];            //
                    cliente.Contato = (string)leitor["Contato"];            //
                    cliente.Email = (string)leitor["Email"];                //
                    
                    clientes.Add(cliente);                                  // Armazena os dados obtidos pelo "leitor" na variável "cliente" (List).
                }
            }
            catch (SqlException ex) // Caso ocorra algum erro e os dados não sejam recuperados e listados no DataGridView.
            {
                msg = "Erro ao consultar dados cadastrados:" + ex.Message;                  // Mensagem de erro e seus detalhes.
                titulo = "Erro...";                                                         // Título do erro.
                MessageBox.Show(msg, titulo, MessageBoxButtons.OK, MessageBoxIcon.Error);   // MessageBox com a mensagem e título. 
            }
            finally
            {
                connection.Close(); // Fecha a conexão com o banco ao terminar a operação
            }
            return clientes;
        }
        //
        // Consultar com parâmetro - Este método tem o intuíto de retornar o cliente cadastrado ao inserir o CNPJ/CPF no campo destino, evitando duplicações.
        //
        public Cliente Consultar(string parametro)  //
        {
            sql = "SELECT * FROM Clientes WHERE CnpjCpf=@CnpjCpf";      // Comando SQL que seleciona o cadastro referente ao numero de CNPJ/CPF digitado.
            Cliente cliente = null;                                     // Instancia um novo cliente sem atributos, é necessário verificar se o CNPJ/CPF consta no banco.
            try
            {
                connection.Open();                                      // Inicia uma conexão com o banco de dados.
                SqlCommand cmd = new SqlCommand(sql, connection);       // Insere o comando SQL solicitado na variável "sql" após a conexão.
                cmd.Parameters.AddWithValue("@CnpjCpf", parametro);     // Parametro do comando SQL.
                SqlDataReader leitor = cmd.ExecuteReader();             // Lê e armazena os dados retornados da consulta na variável "leitor".

                while (leitor.Read())   // Enquanto (while) houver dados para o "leitor" ler e armazenar...
                {
                    if (parametro.Equals(leitor["CnpjCpf"].ToString()))         // Compara se o CNPJ/CPF informado no parametro consta nos registros do banco
                    {
                        cliente = new Cliente();                                // Se constar, inicia os atributos de um novo cliente.

                        cliente.Id = (int)leitor["Id_Cliente"];                 //
                        cliente.CnpjCpf = (string)leitor["CnpjCpf"];            //
                        cliente.Ie = (string)leitor["Ie"];                      //
                        cliente.NomeFantasia = (string)leitor["NomeFantasia"];  //
                        cliente.RazaoSocial = (string)leitor["RazaoSocial"];    //
                        cliente.Cep = (string)leitor["Cep"];                    //
                        cliente.Logradouro = (string)leitor["Logradouro"];      //
                        cliente.Numero = (string)leitor["Numero"];              // 
                        cliente.Complemento = (string)leitor["Complemento"];    // Lê e armazena os dados nas respectivas colunas.
                        cliente.Bairro = (string)leitor["Bairro"];              //
                        cliente.Municipio = (string)leitor["Municipio"];        //
                        cliente.Uf = (string)leitor["Uf"];                      //
                        cliente.CodMun = (string)leitor["CodMun"];              //
                        cliente.Telefone = (string)leitor["Telefone"];          //
                        cliente.Celular = (string)leitor["Celular"];            //
                        cliente.Contato = (string)leitor["Contato"];            //
                        cliente.Email = (string)leitor["Email"];                //
                    }
                }
            }
            catch (SqlException ex) // Caso o CNPJ/CPF esteja incorreto ou não esteja cadastrado
            {
                msg = "CNPJ/CPJ incorreto ou inexistente no Banco de Dados. " + ex.Message; // Mensagem de feedback em caso de erro.
                titulo = "Erro...";                                                         // Título da mensagem de feedback de erro.
                MessageBox.Show(msg, titulo, MessageBoxButtons.OK, MessageBoxIcon.Error);   // Messagebox com a mensagem e título.
            }
            finally
            {
                connection.Close();     // Encerra a conexão com o banco de dados após conclusão da operação.
            }
            return cliente;
        }
        //
        // Excluir
        //
        public void Excluir(Cliente cliente)
        {
            sql = "DELETE FROM Clientes WHERE Id_Cliente=@Id_Cliente";      // Comando em SQL que exclui o cadastro selecionado considerando o ID selecionado.
            try
            {
                connection.Open();                                                                  // Inicia uma conexão com o banco de dados.
                SqlCommand cmd = new SqlCommand(sql, connection);                                   // Instancia um comando SQL, iniciando a variável "sql" após a conexão.
                cmd.Parameters.AddWithValue("@Id_Cliente", cliente.Id);                             // Adiciona o parâmetro do comando SQL, no cado o ID do cliente.
                cmd.ExecuteNonQuery();                                                              // Executa o comando de exclusão no banco de dados.
                msg = "Cliente " + cliente.RazaoSocial + " excluido com sucesso!";                  // Mensagem de feedback após exclusão do cadastro.
                titulo = "Sucesso!";                                                                // Título da mensagem de feedback.
                MessageBox.Show(msg, titulo, MessageBoxButtons.OK, MessageBoxIcon.Information);     // MessageBox com a mensagem e título.
            }
            catch (SqlException ex)
            {
                msg = "Erro ao excluir funcionário!" + ex.Message;                          // Mensagem de feedback em caso de erro.
                titulo = "Erro...";                                                         // Título da mensagem de feedback de erro.
                MessageBox.Show(msg, titulo, MessageBoxButtons.OK, MessageBoxIcon.Error);   // Messagebox com a mensagem e título.
            }
            finally
            {
                connection.Close();     // Encerra a conexão com o banco de dados após conclusão da operação.
            }

        }
        //
        // Salvar
        //
        public void Salvar(Cliente cliente)     // Este método inclui duas funções, Salvar (INSERT) e Atualizar (UPDATE)
        {
            if (cliente.Id != 0)
            {
                // UPDATE: Comando SQL que atualiza os dados de um cliente selecionado

                sql = "UPDATE Clientes SET CnpjCpf=@CnpjCpf, Ie=@Ie, NomeFantasia=@NomeFantasia, RazaoSocial=@RazaoSocial, Cep=@Cep, Logradouro=@Logradouro, " +
                    "Numero=@Numero, Complemento=@Complemento, Bairro=@Bairro, Municipio=@Municipio, Uf=@Uf, CodMun=@CodMun, Telefone=@Telefone, Celular=@Celular, " +
                    "Contato=@Contato, Email=@Email WHERE Id_Cliente=@Id_Cliente";
            }
            else
            {
                // INSERT: Comando SQL para novos cadastros, quando os dados são inseridos sem seleção de clientes

                sql = "INSERT INTO Clientes(CnpjCpf, Ie, NomeFantasia, RazaoSocial, Cep, Logradouro, Numero, Complemento, Bairro, Municipio, Uf, CodMun, Telefone, " +
                    "Celular, Contato, Email) VALUES (@CnpjCpf, @Ie, @NomeFantasia, @RazaoSocial, @Cep, @Logradouro, @Numero, @Complemento, @Bairro, @Municipio, @Uf, " +
                    "@CodMun, @Telefone, @Celular, @Contato, @Email)";
            }

            try
            {
                connection.Open();                                                      // Inicia uma conexão com o banco de dados

                SqlCommand cmd = new SqlCommand(sql, connection);                       // Instancia um comando SQL atribuindo os comandos constantes na váriavel "sql"

                cmd.Parameters.AddWithValue("@Id_Cliente", cliente.Id);                 //
                cmd.Parameters.AddWithValue("@CnpjCpf", cliente.CnpjCpf);               //
                cmd.Parameters.AddWithValue("@Ie", cliente.Ie);                         //
                cmd.Parameters.AddWithValue("@NomeFantasia", cliente.NomeFantasia);     //
                cmd.Parameters.AddWithValue("@RazaoSocial", cliente.RazaoSocial);       //
                cmd.Parameters.AddWithValue("@Cep", cliente.Cep);                       //
                cmd.Parameters.AddWithValue("@Logradouro", cliente.Logradouro);         //
                cmd.Parameters.AddWithValue("@Numero", cliente.Numero);                 //
                cmd.Parameters.AddWithValue("@Complemento", cliente.Complemento);       // Adiciona os valores inseridos conforme os parametros
                cmd.Parameters.AddWithValue("@Bairro", cliente.Bairro);                 //
                cmd.Parameters.AddWithValue("@Municipio", cliente.Municipio);           //
                cmd.Parameters.AddWithValue("@Uf", cliente.Uf);                         //
                cmd.Parameters.AddWithValue("@CodMun", cliente.CodMun);                 //
                cmd.Parameters.AddWithValue("@Telefone", cliente.Telefone);             //
                cmd.Parameters.AddWithValue("@Celular", cliente.Celular);               //
                cmd.Parameters.AddWithValue("@Contato", cliente.Contato);               //
                cmd.Parameters.AddWithValue("@Email", cliente.Email);                   //

                cmd.ExecuteNonQuery();                                                  // Executa o comando correspondente a operação (INSERT/UPDATE)

                msg = "Cliente " + cliente.RazaoSocial + " incluído com sucesso!";                  // Mensagem confirmando o sucesso da operação.
                titulo = "Sucesso!";                                                                // Título da mensagem.
                MessageBox.Show(msg, titulo, MessageBoxButtons.OK, MessageBoxIcon.Information);     // MessageBox com informações sobre a operação.
            }
            catch (SqlException ex)
            {
                msg = "Erro ao incluir cliente. " + ex.Message;                             // Mensagem em caso de erro na operação.
                titulo = "Erro!";                                                           // Título da mensagem.
                MessageBox.Show(msg, titulo, MessageBoxButtons.OK, MessageBoxIcon.Error);   // MessageBox com informações sobre a operação.
            }
            finally
            {
                connection.Close();     // Encerra a conexão com o banco de dados após conclusão da operação.
            }
        }
    }
}
