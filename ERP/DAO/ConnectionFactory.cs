﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ERP.DAO
{
    class ConnectionFactory
    {
        // Método de conexão com o banco de dados
        public SqlConnection GetConnection()
        {
            string servidor = @"Data Source=.\SQLEXPRESS;Initial Catalog=ERP;Integrated Security=True;Pooling=False";
            SqlConnection connection = new SqlConnection(servidor);
            return connection;
        }

    }
}
