﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ERP.DAO
{
    interface IDAO<T>
    {
        void Salvar(T t);
        void Excluir(T t);
        List<T> Consultar();
        T Consultar(string parametro);
    }
}
