﻿using ERP.DAO;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ERP
{
    public partial class formCadCliente : Form
    {
        public formCadCliente()
        {
            InitializeComponent();
        }
        //
        // # MÉTODOS #
        //
        // PREENCHEDADOS - Método para preenchimento do DataGridView
        //
        public void PreencheDados()
        {
            ClienteDAO dao = new ClienteDAO();                      // Instancia uma DAO
            dgvClientes.DataSource = new ClienteDAO().Consultar();  // Preenche o DataGridView
            dgvClientes.ClearSelection();                           // Limpa a seleção do DataGridView
        }
        //
        // CONSULTACEP - Método que consulta e retorna o endereço com base no CEP digitado.
        //
        private void ConsultaCep()
        {
            try
            {
                DataSet ds = new DataSet();                                                     // Cria um conjunto e dados vazio
                string xml = "http://viacep.com.br/ws/@cep/xml/".Replace("@cep", txtCep.Text);  // Efetua a consulta dos dados
                ds.ReadXml(xml);                                                                // Lê os dados retornados da consulta
                txtLogradouro.Text = ds.Tables[0].Rows[0]["logradouro"] as string;              // Preenche o campo Logradouro com os dados obtidos
                txtBairro.Text = ds.Tables[0].Rows[0]["bairro"] as string;                      // Preenche o campo Bairro com os dados obtidos
                txtMunicipio.Text = ds.Tables[0].Rows[0]["localidade"] as string;               // Preenche o campo Municipio com os dados obtidos
                txtCodMun.Text = ds.Tables[0].Rows[0]["ibge"] as string;                        // Preenche o campo Código do Município com os dados obtidos
                txtUF.Text = ds.Tables[0].Rows[0]["uf"] as string;                              // Preenche o campo UF com os dados obtidos
            }
            catch (Exception ex)
            {
            }
        }
        //
        // LIMPAFORM - Método que limpa todos os campos do formulario.
        //
        private void LimpaForm()
        {
            txtCnpjCpf.Clear();
            txtIe.Clear();
            txtNomeFantasia.Clear();
            txtRazaoSocial.Clear();
            txtCep.Clear();
            txtLogradouro.Clear();
            txtNumero.Clear();
            txtComplemento.Clear();
            txtBairro.Clear();
            txtMunicipio.Clear();
            txtUF.Clear();
            txtCodMun.Clear();
            txtTelefone.Clear();
            txtCelular.Clear();
            txtContato.Clear();
            txtEmail.Clear();
            rdoCNPJ.Checked = true;
        }
        //
        // # EVENTOS DE LOAD DO FORMULÁRIO #
        //
        private void CadastroCliente_Load(object sender, EventArgs e)
        {
            rdoCNPJ.Checked = true; // RadioButton "CNPJ" inicia selecionado.
            rdoCNPJ.Focus();        // Foco iniciado no campo onde se preenche o CNPJ ou CPF.
            txtIe.Clear();          // Por padrão, caso o cliente tenha CNPJ, o campo Inscrição Estadual inicia-se em branco.
            PreencheDados();        // Preenche o DataGridView com os dados dos clientes já cadastrados.
        }
        //
        // # FORMATAÇÕES DE CAMPO #
        //
        // Preenchimento automatico do endereço ao incluir o CEP (necessário conexão com a internet).
        //
        private void txtCep_Leave(object sender, EventArgs e)
        {
            ConsultaCep();
        }
        //
        // Ativação da máscara CNPJ ao selecionar o botão correspondente.
        //
        private void rdoCNPJ_CheckedChanged(object sender, EventArgs e)
        {
            txtCnpjCpf.Mask = "00,000,000/0000-00";
            txtIe.Clear();
        }
        //
        // Ativação da máscara CPF ao selecionar o botão correspondente.
        //
        private void rdoCPF_CheckedChanged(object sender, EventArgs e)
        {
            txtCnpjCpf.Mask = "000,000,000-00";
            txtIe.Text = "ISENTO";
        }
        //
        // Preenchimento dos dados do cliente aos respectivos campos ao selecionar o mesmo na lista do DataGridView.
        //
        private void dgvClientes_SelectionChanged(object sender, EventArgs e)
        {
            if (dgvClientes.CurrentRow != null)                                             // Verifica se há alguma linha selecionada na lista
            {
                txtID.Text = dgvClientes.CurrentRow.Cells[0].Value.ToString();              //
                txtCnpjCpf.Text = dgvClientes.CurrentRow.Cells[1].Value.ToString();         //
                txtIe.Text = dgvClientes.CurrentRow.Cells[2].Value.ToString();              //
                txtNomeFantasia.Text = dgvClientes.CurrentRow.Cells[3].Value.ToString();    //
                txtRazaoSocial.Text = dgvClientes.CurrentRow.Cells[4].Value.ToString();     //
                txtCep.Text = dgvClientes.CurrentRow.Cells[5].Value.ToString();             //
                txtLogradouro.Text = dgvClientes.CurrentRow.Cells[6].Value.ToString();      //
                txtNumero.Text = dgvClientes.CurrentRow.Cells[7].Value.ToString();          //
                txtComplemento.Text = dgvClientes.CurrentRow.Cells[8].Value.ToString();     // Preenche os respectivos campos com os dados recuperados do DataGridView 
                txtBairro.Text = dgvClientes.CurrentRow.Cells[9].Value.ToString();          //
                txtMunicipio.Text = dgvClientes.CurrentRow.Cells[10].Value.ToString();      //
                txtUF.Text = dgvClientes.CurrentRow.Cells[11].Value.ToString();             //
                txtCodMun.Text = dgvClientes.CurrentRow.Cells[12].Value.ToString();         //
                txtTelefone.Text = dgvClientes.CurrentRow.Cells[13].Value.ToString();       //
                txtCelular.Text = dgvClientes.CurrentRow.Cells[14].Value.ToString();        //
                txtContato.Text = dgvClientes.CurrentRow.Cells[15].Value.ToString();        //
                txtEmail.Text = dgvClientes.CurrentRow.Cells[16].Value.ToString();          //
            }
        }
        //
        //
        // # EVENTOS DOS BOTÕES #
        //
        // SALVAR
        //
        private void btnSalvar_Click(object sender, EventArgs e)
        {
            // Para que não fique nenhum campo obrigatório em branco, faremos uma verificação.

            if (string.IsNullOrEmpty(txtCnpjCpf.Text) && string.IsNullOrEmpty(txtIe.Text)
                && string.IsNullOrEmpty(txtNomeFantasia.Text) && string.IsNullOrEmpty(txtRazaoSocial.Text)
                && string.IsNullOrEmpty(txtCep.Text) && string.IsNullOrEmpty(txtLogradouro.Text)
                && string.IsNullOrEmpty(txtNumero.Text) && string.IsNullOrEmpty(txtBairro.Text)
                && string.IsNullOrEmpty(txtMunicipio.Text) && string.IsNullOrEmpty(txtUF.Text)
                && string.IsNullOrEmpty(txtCodMun.Text) && string.IsNullOrEmpty(txtTelefone.Text)
                && string.IsNullOrEmpty(txtCelular.Text) && string.IsNullOrEmpty(txtContato.Text)
                && string.IsNullOrEmpty(txtEmail.Text))
            {
                MessageBox.Show("Campos obrigatórios não preenchidos.");    // Mensagem de alerta caso algum campo obrigatório não esteja preenchido.
            }
            else
            {
                Cliente cliente = new Cliente();            // Após a verificação, instancia um novo cliente

                if (!string.IsNullOrEmpty(txtID.Text))      // Verifica se o campo ID esta preenchido (campo com preenchimento automatico)
                {
                    int id = 0;                             // Cria o ID do cliente.

                    if (int.TryParse(txtID.Text, out id))   // Converte o tipo de dado da variavél id de int para string
                    {
                        cliente.Id = id;                    // Atribui o valor do ID do cliente selecionado à variável id acima.
                    }
                }

                cliente.CnpjCpf = txtCnpjCpf.Text;              //
                cliente.Ie = txtIe.Text;                        //
                cliente.NomeFantasia = txtNomeFantasia.Text;    //
                cliente.RazaoSocial = txtRazaoSocial.Text;      //
                cliente.Cep = txtCep.Text;                      //
                cliente.Logradouro = txtLogradouro.Text;        //
                cliente.Numero = txtNumero.Text;                //
                cliente.Complemento = txtComplemento.Text;      //
                cliente.Bairro = txtBairro.Text;                // Atribui os dados preenchidos ao cliente
                cliente.Municipio = txtMunicipio.Text;          //
                cliente.Uf = txtUF.Text;                        //
                cliente.CodMun = txtCodMun.Text;                //
                cliente.Telefone = txtTelefone.Text;            //
                cliente.Celular = txtCelular.Text;              //
                cliente.Contato = txtContato.Text;              //
                cliente.Email = txtEmail.Text;                  //
                
                ClienteDAO dao = new ClienteDAO();  // Instancia um DAO atravéz da classe ClienteDAO onde será executado o comando para salvar.
                dao.Salvar(cliente);                // Salva os dados do cliente no banco de dados
                LimpaForm();                        // Limpa os dados para novo cadastro
                PreencheDados();
            }
        }
        //
        // CANCELAR ** Decidindo se manterei **
        //
        private void btnCancelar_Click(object sender, EventArgs e)
        {
            string msg, titulo;
            msg = "Deseja realmente cancelar o cadastro?";
            titulo = "Cancelar";
            DialogResult escolha = MessageBox.Show(msg,titulo,MessageBoxButtons.YesNo,MessageBoxIcon.Question);
            if (escolha.Equals(DialogResult.Yes))
            {
                Close();
            }
        }
        //
        // EXCLUIR
        //
        private void btnExcluir_Click(object sender, EventArgs e)
        {
            // Obviamente, só podemos excluir um cliente selecionando um dentre os cadastrados. Incluiremos então uma verificação.

            if (string.IsNullOrEmpty(txtID.Text))   // Verifica se o campo ID esta vazio ou é nulo.
            {
                string msg, titulo;                                                             // Seremos alertados ao clicar no botão excluir sem ter selecionado um cliente.
                msg = "Selecione um cliente na lista abaixo.";                                  // Mensagem de feedback informando o erro.
                titulo = "Operação não realizada!";                                             // Título da mensagem.
                MessageBox.Show(msg,titulo,MessageBoxButtons.OK,MessageBoxIcon.Exclamation);    // MessageBox com a mensagem e título.
            }
            else    // Selecionado o cliente desejado
            {
                Cliente cliente = new Cliente();        // Instancia um novo cliente
                int id = 0;                             // Cria um ID para este cliente instanciado

                if (int.TryParse(txtID.Text, out id))   // Converte o tipo de dado da variavél id de int para string
                {
                    cliente.Id = id;                    // Atribui o valor do ID do cliente selecionado à variável id acima.
                }

                ClienteDAO dao = new ClienteDAO();      // Instancia um DAO atravéz da classe ClienteDAO onde será executado o comando de exclusão.

                // Para evitar exclusões acidentais, incluimos um MessageBox para confirmar se há certeza quanto a operação.
                DialogResult resposta = MessageBox.Show("Deseja realmente excluir o cliente selecionado?", "Atenção!", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                if (resposta.Equals(DialogResult.OK))   //Se a resposta for positiva...
                {
                    dao.Excluir(cliente);   // Exclui os dados do cliente selecionado
                    PreencheDados();        // Atualiza os dados exibidos no DataGridView
                }
                
            }
        }
    }
}
