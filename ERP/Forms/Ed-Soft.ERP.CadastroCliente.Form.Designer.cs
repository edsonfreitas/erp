﻿namespace ERP
{
    partial class formCadCliente
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupDadosCadastrais = new System.Windows.Forms.GroupBox();
            this.txtCep = new System.Windows.Forms.TextBox();
            this.txtIe = new System.Windows.Forms.MaskedTextBox();
            this.txtCnpjCpf = new System.Windows.Forms.MaskedTextBox();
            this.txtUF = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.txtCodMun = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.txtMunicipio = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.txtBairro = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.txtComplemento = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.txtNumero = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.txtLogradouro = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.txtRazaoSocial = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtNomeFantasia = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.rdoCPF = new System.Windows.Forms.RadioButton();
            this.rdoCNPJ = new System.Windows.Forms.RadioButton();
            this.txtID = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.btnLimpar = new System.Windows.Forms.Button();
            this.groupContato = new System.Windows.Forms.GroupBox();
            this.txtCelular = new System.Windows.Forms.MaskedTextBox();
            this.txtTelefone = new System.Windows.Forms.MaskedTextBox();
            this.label21 = new System.Windows.Forms.Label();
            this.txtContato = new System.Windows.Forms.TextBox();
            this.txtEmail = new System.Windows.Forms.TextBox();
            this.label26 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.btnCancelar = new System.Windows.Forms.Button();
            this.btnSalvar = new System.Windows.Forms.Button();
            this.btnExcluir = new System.Windows.Forms.Button();
            this.dgvClientes = new System.Windows.Forms.DataGridView();
            this.groupDadosCadastrais.SuspendLayout();
            this.groupContato.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvClientes)).BeginInit();
            this.SuspendLayout();
            // 
            // groupDadosCadastrais
            // 
            this.groupDadosCadastrais.AutoSize = true;
            this.groupDadosCadastrais.Controls.Add(this.txtCep);
            this.groupDadosCadastrais.Controls.Add(this.txtIe);
            this.groupDadosCadastrais.Controls.Add(this.txtCnpjCpf);
            this.groupDadosCadastrais.Controls.Add(this.txtUF);
            this.groupDadosCadastrais.Controls.Add(this.label12);
            this.groupDadosCadastrais.Controls.Add(this.txtCodMun);
            this.groupDadosCadastrais.Controls.Add(this.label11);
            this.groupDadosCadastrais.Controls.Add(this.txtMunicipio);
            this.groupDadosCadastrais.Controls.Add(this.label10);
            this.groupDadosCadastrais.Controls.Add(this.txtBairro);
            this.groupDadosCadastrais.Controls.Add(this.label9);
            this.groupDadosCadastrais.Controls.Add(this.txtComplemento);
            this.groupDadosCadastrais.Controls.Add(this.label8);
            this.groupDadosCadastrais.Controls.Add(this.txtNumero);
            this.groupDadosCadastrais.Controls.Add(this.label7);
            this.groupDadosCadastrais.Controls.Add(this.txtLogradouro);
            this.groupDadosCadastrais.Controls.Add(this.label6);
            this.groupDadosCadastrais.Controls.Add(this.label5);
            this.groupDadosCadastrais.Controls.Add(this.txtRazaoSocial);
            this.groupDadosCadastrais.Controls.Add(this.label4);
            this.groupDadosCadastrais.Controls.Add(this.txtNomeFantasia);
            this.groupDadosCadastrais.Controls.Add(this.label3);
            this.groupDadosCadastrais.Controls.Add(this.label2);
            this.groupDadosCadastrais.Controls.Add(this.rdoCPF);
            this.groupDadosCadastrais.Controls.Add(this.rdoCNPJ);
            this.groupDadosCadastrais.Controls.Add(this.txtID);
            this.groupDadosCadastrais.Controls.Add(this.label1);
            this.groupDadosCadastrais.Location = new System.Drawing.Point(12, 12);
            this.groupDadosCadastrais.Name = "groupDadosCadastrais";
            this.groupDadosCadastrais.Size = new System.Drawing.Size(480, 291);
            this.groupDadosCadastrais.TabIndex = 0;
            this.groupDadosCadastrais.TabStop = false;
            this.groupDadosCadastrais.Text = "Dados Cadastrais";
            // 
            // txtCep
            // 
            this.txtCep.Location = new System.Drawing.Point(404, 98);
            this.txtCep.MaxLength = 8;
            this.txtCep.Name = "txtCep";
            this.txtCep.Size = new System.Drawing.Size(70, 25);
            this.txtCep.TabIndex = 8;
            this.txtCep.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtCep.TextChanged += new System.EventHandler(this.txtCep_Leave);
            // 
            // txtIe
            // 
            this.txtIe.CutCopyMaskFormat = System.Windows.Forms.MaskFormat.ExcludePromptAndLiterals;
            this.txtIe.Location = new System.Drawing.Point(208, 50);
            this.txtIe.Name = "txtIe";
            this.txtIe.Size = new System.Drawing.Size(120, 25);
            this.txtIe.TabIndex = 5;
            this.txtIe.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtIe.TextMaskFormat = System.Windows.Forms.MaskFormat.ExcludePromptAndLiterals;
            // 
            // txtCnpjCpf
            // 
            this.txtCnpjCpf.CutCopyMaskFormat = System.Windows.Forms.MaskFormat.ExcludePromptAndLiterals;
            this.txtCnpjCpf.Location = new System.Drawing.Point(82, 50);
            this.txtCnpjCpf.Name = "txtCnpjCpf";
            this.txtCnpjCpf.Size = new System.Drawing.Size(120, 25);
            this.txtCnpjCpf.TabIndex = 4;
            this.txtCnpjCpf.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtCnpjCpf.TextMaskFormat = System.Windows.Forms.MaskFormat.ExcludePromptAndLiterals;
            // 
            // txtUF
            // 
            this.txtUF.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtUF.Location = new System.Drawing.Point(368, 242);
            this.txtUF.MaxLength = 2;
            this.txtUF.Name = "txtUF";
            this.txtUF.Size = new System.Drawing.Size(30, 25);
            this.txtUF.TabIndex = 14;
            this.txtUF.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(374, 222);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(23, 17);
            this.label12.TabIndex = 55;
            this.label12.Text = "UF";
            this.label12.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // txtCodMun
            // 
            this.txtCodMun.Location = new System.Drawing.Point(404, 242);
            this.txtCodMun.MaxLength = 7;
            this.txtCodMun.Name = "txtCodMun";
            this.txtCodMun.Size = new System.Drawing.Size(70, 25);
            this.txtCodMun.TabIndex = 15;
            this.txtCodMun.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(404, 222);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(68, 17);
            this.label11.TabIndex = 54;
            this.label11.Text = "Cód. Mun.";
            // 
            // txtMunicipio
            // 
            this.txtMunicipio.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtMunicipio.Location = new System.Drawing.Point(6, 242);
            this.txtMunicipio.MaxLength = 30;
            this.txtMunicipio.Name = "txtMunicipio";
            this.txtMunicipio.Size = new System.Drawing.Size(356, 25);
            this.txtMunicipio.TabIndex = 13;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(6, 222);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(65, 17);
            this.label10.TabIndex = 53;
            this.label10.Text = "Municipio";
            // 
            // txtBairro
            // 
            this.txtBairro.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtBairro.Location = new System.Drawing.Point(208, 194);
            this.txtBairro.MaxLength = 15;
            this.txtBairro.Name = "txtBairro";
            this.txtBairro.Size = new System.Drawing.Size(266, 25);
            this.txtBairro.TabIndex = 12;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(208, 174);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(43, 17);
            this.label9.TabIndex = 52;
            this.label9.Text = "Bairro";
            // 
            // txtComplemento
            // 
            this.txtComplemento.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtComplemento.Location = new System.Drawing.Point(6, 194);
            this.txtComplemento.MaxLength = 15;
            this.txtComplemento.Name = "txtComplemento";
            this.txtComplemento.Size = new System.Drawing.Size(196, 25);
            this.txtComplemento.TabIndex = 11;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(6, 174);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(90, 17);
            this.label8.TabIndex = 51;
            this.label8.Text = "Complemento";
            // 
            // txtNumero
            // 
            this.txtNumero.Location = new System.Drawing.Point(404, 146);
            this.txtNumero.MaxLength = 5;
            this.txtNumero.Name = "txtNumero";
            this.txtNumero.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtNumero.Size = new System.Drawing.Size(70, 25);
            this.txtNumero.TabIndex = 10;
            this.txtNumero.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(404, 126);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(56, 17);
            this.label7.TabIndex = 49;
            this.label7.Text = "Numero";
            // 
            // txtLogradouro
            // 
            this.txtLogradouro.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtLogradouro.Location = new System.Drawing.Point(6, 146);
            this.txtLogradouro.MaxLength = 45;
            this.txtLogradouro.Name = "txtLogradouro";
            this.txtLogradouro.Size = new System.Drawing.Size(392, 25);
            this.txtLogradouro.TabIndex = 9;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(6, 126);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(78, 17);
            this.label6.TabIndex = 46;
            this.label6.Text = "Logradouro";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(404, 78);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(30, 17);
            this.label5.TabIndex = 43;
            this.label5.Text = "CEP";
            // 
            // txtRazaoSocial
            // 
            this.txtRazaoSocial.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtRazaoSocial.Location = new System.Drawing.Point(6, 98);
            this.txtRazaoSocial.MaxLength = 35;
            this.txtRazaoSocial.Name = "txtRazaoSocial";
            this.txtRazaoSocial.Size = new System.Drawing.Size(392, 25);
            this.txtRazaoSocial.TabIndex = 7;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(6, 78);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(82, 17);
            this.label4.TabIndex = 41;
            this.label4.Text = "Razão Social";
            // 
            // txtNomeFantasia
            // 
            this.txtNomeFantasia.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtNomeFantasia.Location = new System.Drawing.Point(334, 50);
            this.txtNomeFantasia.MaxLength = 15;
            this.txtNomeFantasia.Name = "txtNomeFantasia";
            this.txtNomeFantasia.Size = new System.Drawing.Size(140, 25);
            this.txtNomeFantasia.TabIndex = 6;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(339, 30);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(95, 17);
            this.label3.TabIndex = 37;
            this.label3.Text = "Nome Fantasia";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(208, 28);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(112, 17);
            this.label2.TabIndex = 33;
            this.label2.Text = "Inscrição Estadual";
            // 
            // rdoCPF
            // 
            this.rdoCPF.AutoSize = true;
            this.rdoCPF.Location = new System.Drawing.Point(143, 28);
            this.rdoCPF.Name = "rdoCPF";
            this.rdoCPF.Size = new System.Drawing.Size(47, 21);
            this.rdoCPF.TabIndex = 3;
            this.rdoCPF.Text = "CPF";
            this.rdoCPF.UseVisualStyleBackColor = true;
            this.rdoCPF.CheckedChanged += new System.EventHandler(this.rdoCPF_CheckedChanged);
            // 
            // rdoCNPJ
            // 
            this.rdoCNPJ.Location = new System.Drawing.Point(82, 28);
            this.rdoCNPJ.Name = "rdoCNPJ";
            this.rdoCNPJ.Size = new System.Drawing.Size(55, 21);
            this.rdoCNPJ.TabIndex = 2;
            this.rdoCNPJ.Text = "CNPJ";
            this.rdoCNPJ.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.rdoCNPJ.UseVisualStyleBackColor = true;
            this.rdoCNPJ.CheckedChanged += new System.EventHandler(this.rdoCNPJ_CheckedChanged);
            // 
            // txtID
            // 
            this.txtID.Location = new System.Drawing.Point(6, 50);
            this.txtID.Name = "txtID";
            this.txtID.Size = new System.Drawing.Size(70, 25);
            this.txtID.TabIndex = 1;
            this.txtID.TabStop = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 30);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(20, 17);
            this.label1.TabIndex = 27;
            this.label1.Text = "ID";
            // 
            // btnLimpar
            // 
            this.btnLimpar.Location = new System.Drawing.Point(608, 256);
            this.btnLimpar.Name = "btnLimpar";
            this.btnLimpar.Size = new System.Drawing.Size(100, 47);
            this.btnLimpar.TabIndex = 28;
            this.btnLimpar.TabStop = false;
            this.btnLimpar.Text = "Limpar Dados";
            this.btnLimpar.UseVisualStyleBackColor = true;
            // 
            // groupContato
            // 
            this.groupContato.AutoSize = true;
            this.groupContato.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.groupContato.Controls.Add(this.txtCelular);
            this.groupContato.Controls.Add(this.txtTelefone);
            this.groupContato.Controls.Add(this.label21);
            this.groupContato.Controls.Add(this.txtContato);
            this.groupContato.Controls.Add(this.txtEmail);
            this.groupContato.Controls.Add(this.label26);
            this.groupContato.Controls.Add(this.label23);
            this.groupContato.Controls.Add(this.label24);
            this.groupContato.Location = new System.Drawing.Point(502, 12);
            this.groupContato.Name = "groupContato";
            this.groupContato.Size = new System.Drawing.Size(480, 147);
            this.groupContato.TabIndex = 0;
            this.groupContato.TabStop = false;
            this.groupContato.Text = "Informação de Contato";
            // 
            // txtCelular
            // 
            this.txtCelular.CutCopyMaskFormat = System.Windows.Forms.MaskFormat.ExcludePromptAndLiterals;
            this.txtCelular.Location = new System.Drawing.Point(132, 50);
            this.txtCelular.Mask = "(00) 00000-0000";
            this.txtCelular.Name = "txtCelular";
            this.txtCelular.Size = new System.Drawing.Size(120, 25);
            this.txtCelular.TabIndex = 17;
            this.txtCelular.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtCelular.TextMaskFormat = System.Windows.Forms.MaskFormat.ExcludePromptAndLiterals;
            // 
            // txtTelefone
            // 
            this.txtTelefone.CutCopyMaskFormat = System.Windows.Forms.MaskFormat.ExcludePromptAndLiterals;
            this.txtTelefone.Location = new System.Drawing.Point(6, 50);
            this.txtTelefone.Mask = "(00) 0000-0000";
            this.txtTelefone.Name = "txtTelefone";
            this.txtTelefone.Size = new System.Drawing.Size(120, 25);
            this.txtTelefone.TabIndex = 16;
            this.txtTelefone.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtTelefone.TextMaskFormat = System.Windows.Forms.MaskFormat.ExcludePromptAndLiterals;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(3, 78);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(167, 17);
            this.label21.TabIndex = 0;
            this.label21.Text = "E-mail para envio de fatura";
            // 
            // txtContato
            // 
            this.txtContato.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtContato.Location = new System.Drawing.Point(258, 50);
            this.txtContato.Name = "txtContato";
            this.txtContato.Size = new System.Drawing.Size(216, 25);
            this.txtContato.TabIndex = 18;
            // 
            // txtEmail
            // 
            this.txtEmail.CharacterCasing = System.Windows.Forms.CharacterCasing.Lower;
            this.txtEmail.Location = new System.Drawing.Point(6, 98);
            this.txtEmail.Name = "txtEmail";
            this.txtEmail.Size = new System.Drawing.Size(468, 25);
            this.txtEmail.TabIndex = 19;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(255, 30);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(54, 17);
            this.label26.TabIndex = 13;
            this.label26.Text = "Contato";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(3, 30);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(57, 17);
            this.label23.TabIndex = 5;
            this.label23.Text = "Telefone";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(132, 30);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(48, 17);
            this.label24.TabIndex = 7;
            this.label24.Text = "Celular";
            // 
            // btnCancelar
            // 
            this.btnCancelar.Location = new System.Drawing.Point(502, 256);
            this.btnCancelar.Name = "btnCancelar";
            this.btnCancelar.Size = new System.Drawing.Size(100, 47);
            this.btnCancelar.TabIndex = 29;
            this.btnCancelar.TabStop = false;
            this.btnCancelar.Text = "Cancelar";
            this.btnCancelar.UseVisualStyleBackColor = true;
            this.btnCancelar.Click += new System.EventHandler(this.btnCancelar_Click);
            // 
            // btnSalvar
            // 
            this.btnSalvar.Location = new System.Drawing.Point(882, 256);
            this.btnSalvar.Name = "btnSalvar";
            this.btnSalvar.Size = new System.Drawing.Size(100, 47);
            this.btnSalvar.TabIndex = 20;
            this.btnSalvar.Text = "Salvar";
            this.btnSalvar.UseVisualStyleBackColor = true;
            this.btnSalvar.Click += new System.EventHandler(this.btnSalvar_Click);
            // 
            // btnExcluir
            // 
            this.btnExcluir.Location = new System.Drawing.Point(714, 256);
            this.btnExcluir.Name = "btnExcluir";
            this.btnExcluir.Size = new System.Drawing.Size(100, 47);
            this.btnExcluir.TabIndex = 30;
            this.btnExcluir.TabStop = false;
            this.btnExcluir.Text = "Excluir";
            this.btnExcluir.UseVisualStyleBackColor = true;
            this.btnExcluir.Click += new System.EventHandler(this.btnExcluir_Click);
            // 
            // dgvClientes
            // 
            this.dgvClientes.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dgvClientes.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dgvClientes.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvClientes.Location = new System.Drawing.Point(12, 320);
            this.dgvClientes.Name = "dgvClientes";
            this.dgvClientes.Size = new System.Drawing.Size(970, 189);
            this.dgvClientes.TabIndex = 31;
            this.dgvClientes.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvClientes_SelectionChanged);
            // 
            // formCadCliente
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 17F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(994, 521);
            this.Controls.Add(this.dgvClientes);
            this.Controls.Add(this.btnExcluir);
            this.Controls.Add(this.btnSalvar);
            this.Controls.Add(this.btnCancelar);
            this.Controls.Add(this.groupDadosCadastrais);
            this.Controls.Add(this.btnLimpar);
            this.Controls.Add(this.groupContato);
            this.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MaximizeBox = false;
            this.Name = "formCadCliente";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = ".:: Cadastro de Clientes ::.";
            this.Load += new System.EventHandler(this.CadastroCliente_Load);
            this.groupDadosCadastrais.ResumeLayout(false);
            this.groupDadosCadastrais.PerformLayout();
            this.groupContato.ResumeLayout(false);
            this.groupContato.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvClientes)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupDadosCadastrais;
        private System.Windows.Forms.TextBox txtUF;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox txtCodMun;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox txtMunicipio;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox txtBairro;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox txtComplemento;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txtNumero;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtLogradouro;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtRazaoSocial;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtNomeFantasia;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.RadioButton rdoCPF;
        private System.Windows.Forms.RadioButton rdoCNPJ;
        private System.Windows.Forms.TextBox txtID;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnLimpar;
        private System.Windows.Forms.GroupBox groupContato;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.TextBox txtContato;
        private System.Windows.Forms.TextBox txtEmail;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Button btnCancelar;
        private System.Windows.Forms.Button btnSalvar;
        private System.Windows.Forms.MaskedTextBox txtCnpjCpf;
        private System.Windows.Forms.MaskedTextBox txtIe;
        private System.Windows.Forms.MaskedTextBox txtCelular;
        private System.Windows.Forms.MaskedTextBox txtTelefone;
        private System.Windows.Forms.Button btnExcluir;
        private System.Windows.Forms.TextBox txtCep;
        private System.Windows.Forms.DataGridView dgvClientes;
    }
}