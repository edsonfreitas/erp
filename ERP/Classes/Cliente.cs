﻿namespace ERP
{
    class Cliente
    {
        public Cliente()
        {
        }
        //
        // Propriedades
        //
        public int Id { get; set; }
        public string CnpjCpf { get; set; }
        public string Ie { get; set; }
        public string NomeFantasia { get; set; }
        public string RazaoSocial { get; set; }
        public string Cep { get; set; }
        public string Logradouro { get; set; }
        public string Numero { get; set; }
        public string Complemento { get; set; }
        public string Bairro { get; set; }
        public string Municipio { get; set; }
        public string Uf { get; set; }
        public string CodMun { get; set; }
        public string Telefone { get; set; }
        public string Celular { get; set; }
        public string Contato { get; set; }
        public string Email { get; set; }

    }
}